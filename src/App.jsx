import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import { Error, ErrorElement } from './components';
import {
    HomeLayout,
    BrandManagement,
    CategoryManagement,
    DashboardLanding,
    OrderManagement,
    ProductManagement,
    Signin,
    UserManagement,
} from './pages';

const router = createBrowserRouter([
    {
        path: '/',
        element: <HomeLayout />,
        errorElement: <Error />,
        children: [
            {
                index: true,
                element: <DashboardLanding />,
                errorElement: <ErrorElement />,
            },
            {
                path: 'admin/brands',
                element: <BrandManagement />,
                errorElement: <ErrorElement />,
            },
            {
                path: 'admin/categories',
                element: <CategoryManagement />,
                errorElement: <ErrorElement />,
            },
            {
                path: 'admin/orders',
                element: <OrderManagement />,
                errorElement: <ErrorElement />,
            },
            { path: 'admin/products', element: <ProductManagement />, errorElement: <ErrorElement /> },

            {
                path: 'admin/users',
                element: <UserManagement />,
                errorElement: <ErrorElement />,
            },
        ],
    },
    {
        path: 'auth/admin/login',
        element: <Signin />,
        errorElement: <ErrorElement />,
    },
]);

function App() {
    return <RouterProvider router={router} />;
}

export default App;
