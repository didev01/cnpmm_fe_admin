import React from 'react';
import { useState } from 'react';
const FormAddItem = ({ onAddAttribute }) => {
    const [name, setName] = useState('');

    const handleAddAttribute = () => {
        if (!name) {
            return;
        }
        const newAttribute = {
            name,
        };
        onAddAttribute(newAttribute);
        setName('');
    };

    return (
        <div>
            <div className="form-control grid grid-cols-2 gap-2">
                <input
                    type="text"
                    value={name}
                    onChange={(event) => setName(event.target.value)}
                    placeholder="Tên thuộc tính"
                    className="input input-bordered input-primary w-full max-w-xs"
                />
                <button type="button" onClick={handleAddAttribute} className="btn w-20">
                    Thêm
                </button>
            </div>
        </div>
    );
};

export default FormAddItem;
