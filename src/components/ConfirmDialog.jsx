import React from 'react';

const ConfirmDialog = ({ onConfirm, onCancel, isVisible }) => {
    return (
        <dialog id="confirmed_dialog" className="modal" open="true">
            <div className="modal-box">
                <h3 className="font-bold text-lg">Xác nhận!</h3>
                <p className="py-4">Bạn có chắc chắn muốn xóa mục này không?</p>
                <div className="modal-action">
                    <button className="btn" onClick={onConfirm}>
                        Xác nhận
                    </button>
                    <button className="btn" onClick={onCancel}>
                        Hủy
                    </button>
                </div>
            </div>
        </dialog>
    );
};

export default ConfirmDialog;
