import React, { useState, useEffect } from 'react';
import { User, BarChart, ShoppingBasket, MenuSquare, ListOrdered, ArrowRight, ArrowLeft, Gem } from 'lucide-react';
import { NavLink } from 'react-router-dom';
import { motion } from 'framer-motion';
import { useTranslation } from 'react-i18next';
import Logo from '../assets/logo/Logo.png';

const variants = {
    expanded: { width: '12%' },
    nonexpanded: { width: '6%' },
};

const transition = {
    type: 'spring',
    damping: 20,
    stiffness: 100,
};

const Sidebar = () => {
    const [isExpanded, setIsExpanded] = useState(true);
    const { t } = useTranslation('translation');
    const navlinks = [
        {
            title: 'Dashboard',
            items: [{ id: 1, link: 'Dashboard', icon: <BarChart />, to: '/' }],
        },
        {
            title: 'Categories & Brands',
            items: [
                { id: 2, link: t('categories'), icon: <MenuSquare />, to: 'admin/categories' },
                { id: 3, link: t('brands'), icon: <Gem />, to: 'admin/brands' },
            ],
        },
        {
            title: 'Products',
            items: [{ id: 5, link: t('products'), icon: <ShoppingBasket />, to: 'admin/products' }],
        },
        {
            title: 'Orders & Sales',
            items: [{ id: 7, link: t('orders'), icon: <ListOrdered />, to: 'admin/orders' }],
        },
        {
            title: 'Users',
            items: [{ id: 9, link: t('users'), icon: <User />, to: 'admin/users' }],
        },
    ];

    const [selectedItem, setSelectedItem] = useState(null);

    const handleLinkClick = (item) => {
        setSelectedItem(item);
    };

    useEffect(() => {
        if (selectedItem === null) {
            setSelectedItem(1);
        }
    }, []);

    const handleToggleSidebar = () => {
        setIsExpanded(!isExpanded);
    };

    return (
        <motion.div
            animate={isExpanded ? 'expanded' : 'nonexpanded'}
            variants={variants}
            transition={transition}
            className={
                'flex flex-col border border-r-1 fixed sidebar h-full bg-[#FDFDFD] ' +
                (isExpanded ? 'md:w-1/4 lg:w-1/5 px-4 py-8' : 'w-1/3 md:w-1/6 p-6 ')
            }
        >
            <div
                onClick={handleToggleSidebar}
                className="cursor-pointer absolute -right-3 top-10 rounded-full w-6 h-6 bg-primary flex justify-center items-center"
            >
                {isExpanded ? <ArrowLeft className="w-10 text-white" /> : <ArrowRight className="w-10 text-white" />}
            </div>
            <div className="mb-6">
                <div className="mx-10">
                    <img src={Logo} alt="" className="w-[100px]" />
                </div>
            </div>
            <div className="flex flex-col space-y-3">
                {navlinks.map((group) => (
                    <div key={group.title}>
                        {isExpanded && <div className="text-sm font-semibold text-gray-600 p-2">{group.title}</div>}
                        {group.items.map((nav) => (
                            <NavLink
                                key={nav.id}
                                to={nav.to}
                                className={`nav-links w-full rounded-lg  ${
                                    selectedItem === nav.id ? 'bg-secondary text-white' : ''
                                }`}
                                onClick={() => handleLinkClick(nav.id)}
                            >
                                <div
                                    className={`${
                                        selectedItem === nav.id ? 'bg-primary hover:bg-primary text-white' : ''
                                    } flex space-x-3 w-full p-2 rounded hover:bg-base-200 hover:text`}
                                >
                                    {nav.icon}
                                    <span className={!isExpanded ? 'hidden' : 'block'}>{nav.link}</span>
                                </div>
                            </NavLink>
                        ))}
                    </div>
                ))}
                {!isExpanded && <div className="text-sm font-semibold text-gray-600 p-2">&nbsp;</div>}
            </div>
        </motion.div>
    );
};

export default Sidebar;
