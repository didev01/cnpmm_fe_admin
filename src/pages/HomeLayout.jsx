import React from 'react';
import { Outlet, useNavigation } from 'react-router-dom';
import { Sidebar, Header } from '../components';
const HomeLayout = () => {
    return (
        <>
            <main className="">
                <div className="flex relative">
                    <Sidebar />
                    <div className="min-h-screen w-full ml-[240px]">
                        <Header />
                        <Outlet />
                    </div>
                </div>
            </main>
        </>
    );
};

export default HomeLayout;
