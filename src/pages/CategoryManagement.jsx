import React, { useEffect, useState, useMemo } from 'react';
import DataTable from 'react-data-table-component';
import { FormInput, Loading, SubmitButton, FormAddItem, TableLoader } from '../components';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import { toast } from 'react-toastify';
import { useSelector, useDispatch } from 'react-redux';
import { FolderEdit, Trash } from 'lucide-react';
import PreviewImage from '../utils/helpers';
import categoryService from '../services/categoryService';
import { useTranslation } from 'react-i18next';

const CategoryManagement = () => {
    const navigate = useNavigate();
    const [data, setData] = useState([]);
    const { t } = useTranslation('translation');
    const [isUpdateMode, setIsUpdateMode] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [pending, setPending] = useState(true);
    const [selectedCategory, setSelectedCategory] = useState(null);
    const [selectedCategoryId, setSelectedCategoryId] = useState(null);
    const [productAttributesList, setProductAttributesList] = useState([]);
    const token = useSelector((state) => state.auth.loginAdmin?.token);

    useEffect(() => {
        if (!token) {
            navigate('/auth/admin/login');
        } else {
            fetchData();
        }
    }, []);

    const fetchData = async () => {
        setPending(true);
        try {
            const resp = await categoryService.getAllCategories(token);
            setData(resp.data);
            setPending(false);
        } catch (error) {
            console.log(error);
        }
    };

    //Search
    const [filterText, setFilterText] = useState('');
    const [resetPaginationToggle, setResetPaginationToggle] = useState(false);
    const filteredItems = data.filter(
        (item) => item.name && item.name.toLowerCase().includes(filterText.toLowerCase()),
    );
    const subHeaderComponentMemo = useMemo(() => {
        return (
            <div className="grid grid-cols-2 my-2">
                <div className="relative">
                    <label className="input-group w-full">
                        <input
                            value={filterText}
                            onChange={(e) => setFilterText(e.target.value)}
                            placeholder="Nhập tên danh mục..."
                            className="input input-bordered max-w-xs"
                        />
                    </label>
                </div>
                <div className="flex items-center mx-5">
                    <button
                        className="btn bg-primary btn-ghost text-white"
                        onClick={() => {
                            setIsUpdateMode(false);
                            document.getElementById('dialog').showModal();
                        }}
                    >
                        + {t('add_cate')}
                    </button>
                </div>
            </div>
        );
    }, [filterText, resetPaginationToggle]);

    const closeDialog = () => {
        document.getElementById('dialog').close();
        setProductAttributesList([]);
        formik.resetForm();
    };
    const handleUpdate = async (id) => {
        try {
            const resp = await categoryService.getCategoryById(id);
            setSelectedCategory(resp.data);
            setIsUpdateMode(true);
            document.getElementById('dialog').showModal();
        } catch (error) {
            console.log(error);
        }
    };
    const handleDeleteModal = (id) => {
        document.getElementById('dialog_confirm').showModal();
        setSelectedCategoryId(id);
    };
    const handleDelete = async () => {
        setIsLoading(true);
        try {
            const resp = await categoryService.deleteCategory(token, selectedCategoryId);
            if (resp.isSuccess) {
                setIsLoading(false);
                toast.success(resp.message);
                fetchData();
            }
        } catch (error) {
            setIsLoading(false);
            if (error.response && error.response.data && error.response.data.message) {
                toast.error(error.response.data.message);
            } else {
                toast.error('An unexpected error occurred.');
            }
        }
    };
    const handleAddAttribute = (newAttribute) => {
        setProductAttributesList([...productAttributesList, newAttribute]);
    };
    const handleDeleteAttribute = (index) => {
        const updatedAttributesList = productAttributesList.filter((_, i) => i !== index);
        setProductAttributesList(updatedAttributesList);
    };
    const columns = [
        {
            name: 'ID',
            selector: (row) => row.id,
            sortable: true,
        },
        {
            name: 'Hình ảnh',
            selector: (row) => <img src={row.image} className="w-20 h-20 my-2"></img>,
            sortable: false,
        },
        {
            name: 'Tên',
            selector: (row) => <div className="text-sm">{row.name}</div>,
            sortable: false,
        },
        {
            name: 'Actions',
            cell: (row) => (
                <>
                    <button
                        className="btn btn-outline btn-success m-2"
                        onClick={() => {
                            handleUpdate(row.id);
                        }}
                    >
                        <FolderEdit />
                    </button>
                    <button
                        className="btn btn-outline btn-error mx-2"
                        onClick={() => {
                            handleDeleteModal(row.id);
                        }}
                    >
                        <Trash />
                    </button>
                </>
            ),
        },
    ];
    const handleSubmit = async (values) => {
        setIsLoading(true);
        try {
            if (isUpdateMode) {
                if (selectedCategory) {
                    try {
                        closeDialog();
                        const info = {
                            name: values.name,
                            description: values.description,
                        };

                        const formData = new FormData();

                        formData.append('info', JSON.stringify(info));
                        formData.append('image', values.file);

                        // Chuyển danh sách thuộc tính thành chuỗi JSON
                        formData.append('productAttributesString', JSON.stringify(productAttributesList));

                        const resp = await categoryService.updateCategory(token, selectedCategory.id, formData);
                        if (resp.isSuccess) {
                            setIsLoading(false);
                            toast.success('Cập nhật thành công!');
                            fetchData();
                        }
                    } catch (error) {
                        setIsLoading(false);
                        if (error.response && error.response.data && error.response.data.messages) {
                            const errorMessages = error.response.data.messages;
                            toast.error(errorMessages.join(', '));
                        } else {
                            toast.error('Có lỗi xảy ra.');
                        }
                    }
                }
            } else {
                closeDialog();
                const info = {
                    name: values.name,
                    description: values.description,
                };

                const formData = new FormData();

                formData.append('info', JSON.stringify(info));
                formData.append('image', values.file);

                // Chuyển danh sách thuộc tính thành chuỗi JSON
                formData.append('productAttributesString', JSON.stringify(productAttributesList));

                const resp = await categoryService.addNewCategory(token, formData);

                if (resp.isSuccess) {
                    setIsLoading(false);
                    toast.success('Thêm dữ liệu thành công!');
                    fetchData();
                } else {
                    toast.error(resp.message || 'Có lỗi xảy ra khi thêm danh mục.');
                }
            }
        } catch (error) {
            toast.error('Có lỗi xảy ra khi gửi yêu cầu.');
            console.error(error);
        } finally {
            setIsLoading(false);
        }
    };

    const formik = useFormik({
        initialValues: {
            name: '',
            description: '',
            file: '',
            productAttributesString: '',
        },
        validationSchema: isUpdateMode
            ? Yup.object({})
            : Yup.object({
                  name: Yup.string().required('Vui lòng nhập thông tin!'),
                  description: Yup.string().required('Vui lòng nhập thông tin!'),
                  file: Yup.mixed().required('Vui lòng tải lên hình ảnh!'),
              }),
        onSubmit: handleSubmit,
    });
    const ExpandedComponent = ({ data }) => {
        return (
            <div className="mx-20">
                <div className="mb-2">
                    <strong>ID:</strong> {data.id}
                </div>
                <div className="mb-2">
                    <strong>Hình ảnh:</strong> <img src={data.image} className="w-20 h-20" alt="Product" />
                </div>
                <div className="mb-2">
                    <strong>Tên:</strong> <div className="text-sm">{data.name}</div>
                </div>
                <div className="mb-2">
                    <strong>Mô tả:</strong> <div className="text-sm">{data.description}</div>
                </div>
                <div>
                    <strong>Thuộc tính:</strong>
                    {data.productAttributes.map((attribute) => (
                        <div key={attribute.id} className="my-1 text-sm">
                            {attribute.name}
                        </div>
                    ))}
                </div>
            </div>
        );
    };
    return (
        <div className="m-10">
            {isLoading ? (
                <Loading />
            ) : (
                <>
                    <dialog id="dialog_confirm" className="modal">
                        <div className="modal-box max-w-lg">
                            <h3 className="font-bold text-2xl text-center">XÓA THƯƠNG HIỆU</h3>
                            <form className="my-2" onSubmit={handleDelete}>
                                <div
                                    onClick={() => document.getElementById('dialog_confirm').close()}
                                    className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2"
                                >
                                    X
                                </div>
                                <div className="text-center space-x-10">
                                    <p className="my-10">Bạn chắc chắn xóa?</p>
                                    <div className="flex items-center mt-3 text-center justify-center">
                                        <button className="btn btn-primary text-white">Xác nhận</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </dialog>
                    <dialog id="dialog" className="modal">
                        <div className="modal-box max-w-2xl">
                            <h3 className="font-bold text-2xl text-center">DANH MỤC SẢN PHẨM</h3>
                            <form className="my-2" onSubmit={formik.handleSubmit}>
                                <div
                                    onClick={closeDialog}
                                    className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2"
                                >
                                    X
                                </div>
                                <div className="space-x-10">
                                    <div>
                                        <FormInput
                                            type="text"
                                            label="Tên Danh mục"
                                            name="name"
                                            value={formik.values.name}
                                            placeholder="Nhập tên danh mục..."
                                            onchange={formik.handleChange}
                                        />
                                        {formik.errors.name && (
                                            <span className="text-error text-sm p-1 ">{formik.errors.name}</span>
                                        )}
                                        <FormInput
                                            type="text"
                                            label="Mô tả"
                                            name="description"
                                            value={formik.values.description}
                                            placeholder="Nhập mô tả..."
                                            onchange={formik.handleChange}
                                        />
                                        {formik.errors.description && (
                                            <span className="text-error text-sm p-1 ">{formik.errors.description}</span>
                                        )}
                                        <FormInput
                                            type="file"
                                            label="Hình ảnh"
                                            name="file"
                                            onchange={(e) => formik.setFieldValue('file', e.target.files[0])}
                                        />
                                        {formik.errors.file && (
                                            <p className="text-error text-sm p-1">{formik.errors.file}</p>
                                        )}
                                        {formik.values.file && <PreviewImage file={formik.values.file} />}
                                        <div className="my-2">
                                            <h4 className="text-sm">Thuộc Tính Sản Phẩm:</h4>
                                            <FormAddItem onAddAttribute={handleAddAttribute} />
                                            <ul className="mx-10">
                                                {productAttributesList.map((attribute, index) => (
                                                    <li key={index} className="my-4">
                                                        <div className="flex items-center justify-between">
                                                            <p>{attribute.name}</p>
                                                            <button onClick={() => handleDeleteAttribute(index)}>
                                                                Xóa
                                                            </button>
                                                        </div>
                                                        <div className="divider"></div>
                                                    </li>
                                                ))}
                                            </ul>
                                        </div>
                                        <div className="flex items-center mt-3 text-center justify-center">
                                            <SubmitButton text={isUpdateMode ? 'Cập nhật' : 'Thêm'} color="primary" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </dialog>
                    <div className="container">
                        <DataTable
                            title="QUẢN LÝ DANH MỤC"
                            fixedHeader
                            fixedHeaderScrollHeight="550px"
                            direction="auto"
                            responsive
                            pagination
                            columns={columns}
                            data={filteredItems}
                            striped
                            subHeader
                            paginationResetDefaultPage={resetPaginationToggle}
                            subHeaderComponent={subHeaderComponentMemo}
                            persistTableHead
                            expandableRows
                            expandableRowsComponent={ExpandedComponent}
                            progressPending={pending}
                            progressComponent={<TableLoader />}
                            customStyles={{
                                table: {
                                    fontSize: '30px',
                                },
                            }}
                        />
                    </div>
                </>
            )}
        </div>
    );
};

export default CategoryManagement;
