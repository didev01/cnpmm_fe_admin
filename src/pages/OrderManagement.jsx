import React, { useEffect, useState, useMemo } from 'react';
import DataTable from 'react-data-table-component';
import { Loading, SubmitButton, TableLoader } from '../components';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useSelector } from 'react-redux';
import { TbStatusChange } from 'react-icons/tb';
import { formatPrice } from '../utils/helpers';
import orderService from '../services/orderService';
import { GoCreditCard } from 'react-icons/go';

const OrderManagement = () => {
    const navigate = useNavigate();
    const [data, setData] = useState([]);
    const [selectedOrderStatus, setSelectedOrderStatus] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [pending, setPending] = useState(true);
    const [selectedOrder, setSelectedOrder] = useState(null);
    const [selectedOrderPaid, setSelectedOrderPaid] = useState(null);
    const token = useSelector((state) => state.auth.loginAdmin?.token);

    useEffect(() => {
        if (!token) {
            navigate('/auth/admin/login');
        } else {
            fetchData();
        }
    }, []);

    const fetchData = async () => {
        setPending(true);
        try {
            const resp = await orderService.getAllOrders(token);
            setData(resp.data);
            setPending(false);
        } catch (error) {
            console.log(error);
        }
    };

    const [filterText, setFilterText] = useState('');
    const [resetPaginationToggle, setResetPaginationToggle] = useState(false);
    const filteredItems = data.filter(
        (item) => item.receiverName && item.receiverName.toLowerCase().includes(filterText.toLowerCase()),
    );
    const subHeaderComponentMemo = useMemo(() => {
        return (
            <div className="">
                <div className="relative">
                    <label className="input-group w-full">
                        <input
                            value={filterText}
                            onChange={(e) => setFilterText(e.target.value)}
                            placeholder="Nhập tên khách hàng..."
                            className="input input-bordered max-w-xs"
                        />
                    </label>
                </div>
            </div>
        );
    }, [filterText, resetPaginationToggle]);

    const closeDialog = () => {
        document.getElementById('dialog').close();
        setIsConfirmationDialogOpen(false);
    };

    const handleUpdateStatus = (id) => {
        setSelectedOrder(id);
        setSelectedOrderStatus(selectedOrder.status); // Set initial status
        document.getElementById('dialog').showModal();
    };
    const handleUpdatePaid = (id) => {
        setSelectedOrderPaid(id);
        document.getElementById('dialog_paid').showModal();
    };

    const handlePaidUpdate = async () => {
        setIsLoading(true);
        try {
            document.getElementById('dialog_paid').close();
            const resp = await orderService.updatePaid(selectedOrderPaid);
            if (resp.isSuccess) {
                toast.success('Thay đổi trạng thái thành công!');
                fetchData();
                setIsLoading(false);
            } else {
                console.error(resp.message); // Log the error message
                // Handle the error appropriately, e.g., show a user-friendly message
                toast.error('Có lỗi xảy ra khi cập nhật trạng thái thanh toán.');
            }
        } catch (error) {
            setIsLoading(false);
            console.error(error);
            // Handle network or unexpected errors here
            toast.error('Có lỗi xảy ra khi gửi yêu cầu.');
        }
    };

    const handleStatusUpdate = async (e) => {
        setIsLoading(true);
        e.preventDefault();

        try {
            const resp = await orderService.updateOrderStatus(token, selectedOrder, selectedOrderStatus);
            if (resp.isSuccess) {
                toast.success('Thay đổi trạng thái thành công!');
                fetchData();
                setIsLoading(false);
                closeDialog();
            }
        } catch (error) {
            setIsLoading(false);
            console.error(error);
        }
    };

    const columns = [
        {
            name: 'ID',
            selector: (row) => row.id,
            sortable: true,
            width: '270px',
        },
        {
            name: 'Ngày tạo',
            selector: (row) => <div className="text-sm">{row.createdAt}</div>,
            sortable: false,
            width: '170px',
        },
        {
            name: 'Ngày cập nhật',
            selector: (row) => <div className="text-sm">{row.lastUpdatedAt}</div>,
            sortable: false,
            width: '170px',
        },
        {
            name: 'Trạng thái',
            selector: (row) => <div className="text-sm">{row.status}</div>,
            sortable: false,
            width: '100px',
        },
        {
            name: 'Tổng tiền',
            selector: (row) => <div className="text-sm">{formatPrice(row.total)}</div>,
            sortable: false,
            width: '130px',
        },
        {
            name: 'Người nhận',
            selector: (row) => <div className="text-sm">{row.receiverName}</div>,
            sortable: false,
            width: '170px',
        },
        {
            name: 'Số điện thoại',
            selector: (row) => <div className="text-sm">{row.receiverPhone}</div>,
            sortable: false,
            width: '120px',
        },
        {
            name: 'Địa chỉ',
            selector: (row) => <div className="text-sm">{row.address}</div>,
            sortable: false,
            width: '120px',
        },
        {
            name: 'Trạng thái đơn/thanh toán',
            cell: (row) => (
                <>
                    <button
                        className="btn btn-outline btn-success m-2"
                        onClick={() => {
                            handleUpdateStatus(row.id);
                        }}
                    >
                        <TbStatusChange />
                    </button>
                    <button
                        className="btn btn-outline btn-success m-2"
                        onClick={() => {
                            handleUpdatePaid(row.id);
                        }}
                    >
                        <GoCreditCard />
                    </button>
                </>
            ),
            width: '200px',
        },
    ];

    const ExpandedComponent = ({ data }) => {
        return (
            <div className="mx-20 ">
                <div className="mb-2">
                    <strong>Mã đơn hàng:</strong> {data.id}
                </div>
                <div className="mb-2">
                    <strong>Danh sách sản phẩm:</strong>
                    <div className="grid grid-cols-2 gap-2">
                        {data.orderItemList.map((item) => (
                            <div>
                                <div className="mb-2">
                                    <div className="mb-2">
                                        <strong>ID:</strong> <div className="text-sm">{item.id}</div>
                                    </div>
                                    <div className="mb-2">
                                        <strong>Tên:</strong> <div className="text-sm">{item.productName}</div>
                                    </div>
                                    <div className="mb-2">
                                        <strong>Hình ảnh:</strong> <img src={item.productImage} className="w-20 h-20" />
                                    </div>
                                    <div className="mb-2">
                                        <strong>Số lượng:</strong> <div className="text-sm">{item.quantity}</div>
                                    </div>
                                    <div className="mb-2">
                                        <strong>Tổng:</strong> <div className="text-sm">{item.total}</div>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
                <div>
                    <strong>Thông tin nhận hàng</strong>
                    <div className="mb-2">
                        <strong>Người nhận:</strong>{' '}
                        <div className="text-sm">
                            {data.receiverName},{data.receiverPhone}
                        </div>
                    </div>
                    <div className="mb-2">
                        <strong>Địa chỉ:</strong> <div className="text-sm">{data.address}</div>
                    </div>
                    <div className="mb-2">
                        <strong>Phương thức thanh toán:</strong> <div className="text-sm">{data.payment}</div>
                    </div>
                    <div className="mb-2">
                        <strong>Trạng thái thanh toán:</strong>{' '}
                        <div className="text-sm">{data.isPaid ? <>Đã thanh toán</> : <>Chờ thanh toán</>}</div>
                    </div>
                </div>
            </div>
        );
    };
    return (
        <div className="m-10">
            {isLoading ? (
                <Loading />
            ) : (
                <>
                    <dialog id="dialog_paid" className="modal">
                        <div className="modal-box max-w-2xl">
                            <h3 className="font-bold text-2xl text-center">CẬP NHẬT TRẠNG THÁI THANH TOÁN</h3>
                            <form className="my-2" onSubmit={handlePaidUpdate}>
                                <div
                                    onClick={closeDialog}
                                    className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2"
                                >
                                    X
                                </div>
                                <div className="text-center space-x-10">
                                    <p className="my-10">Đơn hàng đã được thanh toán?</p>
                                    <div className="flex items-center mt-3 text-center justify-center">
                                        <SubmitButton text="Cập nhật" color="primary" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </dialog>
                    <dialog id="dialog" className="modal">
                        <div className="modal-box">
                            <form onSubmit={handleStatusUpdate}>
                                <form method="dialog">
                                    <button className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2">
                                        ✕
                                    </button>
                                </form>
                                <h3 className="font-bold text-lg text-center uppercase">
                                    Cập nhật trạng thái đơn hàng
                                </h3>
                                <div className="flex items-center justify-center my-10">
                                    <select
                                        className="select select-bordered w-full max-w-xs"
                                        value={selectedOrderStatus}
                                        onChange={(e) => setSelectedOrderStatus(e.target.value)}
                                    >
                                        <option disabled value="">
                                            Trạng thái đơn
                                        </option>
                                        <option value="Đã xác nhận">Đã xác nhận</option>
                                        <option value="Đang giao">Đang giao</option>
                                        <option value="Đã giao">Đã giao</option>
                                        <option value="Đã hủy">Đã hủy</option>
                                    </select>
                                </div>
                                <SubmitButton text="Cập nhật" color="primary" />
                            </form>
                        </div>
                    </dialog>

                    <div className="container">
                        <DataTable
                            title="QUẢN LÝ ĐƠN HÀNG"
                            fixedHeader
                            fixedHeaderScrollHeight="550px"
                            direction="auto"
                            responsive
                            pagination
                            columns={columns}
                            data={filteredItems}
                            striped
                            subHeader
                            paginationResetDefaultPage={resetPaginationToggle}
                            subHeaderComponent={subHeaderComponentMemo}
                            persistTableHead
                            expandableRows
                            expandableRowsComponent={ExpandedComponent}
                            progressPending={pending}
                            progressComponent={<TableLoader />}
                            customStyles={{
                                table: {
                                    fontSize: '30px',
                                },
                            }}
                        />
                    </div>
                </>
            )}
        </div>
    );
};

export default OrderManagement;
