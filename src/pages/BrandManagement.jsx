import React, { useEffect, useState, useMemo } from 'react';
import DataTable from 'react-data-table-component';
import { FormInput, Loading, SubmitButton, TableLoader } from '../components';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import { toast } from 'react-toastify';
import { useSelector, useDispatch } from 'react-redux';
import { FolderEdit, Trash } from 'lucide-react';
import PreviewImage from '../utils/helpers';
import brandService from '../services/brandService';

const BrandManagement = () => {
    const navigate = useNavigate();
    const [data, setData] = useState([]);
    const [isUpdateMode, setIsUpdateMode] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [pending, setPending] = useState(true);
    const [selectedBrandId, setSelectedBrandId] = useState(null);
    const [selectedBrand, setSelectedBrand] = useState(null);
    const token = useSelector((state) => state.auth.loginAdmin?.token);

    useEffect(() => {
        if (!token) {
            navigate('/auth/admin/login');
        } else {
            fetchData();
        }
    }, []);

    const fetchData = async () => {
        setPending(true);
        try {
            const resp = await brandService.getAllBrands(token);
            setData(resp.data);
            setPending(false);
        } catch (error) {
            console.log(error);
        }
    };

    //Search
    const [filterText, setFilterText] = useState('');
    const [resetPaginationToggle, setResetPaginationToggle] = useState(false);
    const filteredItems = data.filter(
        (item) => item.name && item.name.toLowerCase().includes(filterText.toLowerCase()),
    );
    const subHeaderComponentMemo = useMemo(() => {
        return (
            <div className="grid grid-cols-2 my-2">
                <div className="relative">
                    <label className="input-group w-full">
                        <input
                            value={filterText}
                            onChange={(e) => setFilterText(e.target.value)}
                            placeholder="Nhập từ khóa tìm kiếm..."
                            className="input input-bordered max-w-xs"
                        />
                    </label>
                </div>
                <div className="flex items-center mx-5">
                    <button
                        className="btn bg-primary btn-ghost text-white"
                        onClick={() => {
                            setIsUpdateMode(false);
                            document.getElementById('dialog').showModal();
                        }}
                    >
                        + Thêm thương hiệu
                    </button>
                </div>
            </div>
        );
    }, [filterText, resetPaginationToggle]);

    const closeDialog = () => {
        document.getElementById('dialog').close();
        setSelectedBrand(null);
        formik.resetForm();
    };
    const handleDeleteModal = (id) => {
        document.getElementById('dialog_confirm').showModal();
        setSelectedBrandId(id);
    };
    const handleDelete = async () => {
        setIsLoading(true);
        try {
            const resp = await brandService.deleteBrand(token, selectedBrandId);
            if (resp.isSuccess) {
                setIsLoading(false);
                toast.success('Xóa dữ liệu thành công');
                fetchData();
            }
        } catch (error) {
            setIsLoading(false);
            if (error.response && error.response.data && error.response.data.message) {
                toast.error(error.response.data.message);
            } else {
                toast.error('An unexpected error occurred.');
            }
        }
    };
    const handleUpdate = async (id) => {
        try {
            const resp = await brandService.getBrandById(id);
            setSelectedBrand(resp.data);
            setIsUpdateMode(true);
            formik.resetForm();
            document.getElementById('dialog').showModal();
        } catch (error) {
            console.log(error);
        }
    };

    const handleSubmit = async (values) => {
        setIsLoading(true);
        if (isUpdateMode) {
            if (selectedBrand) {
                try {
                    closeDialog();
                    const formData = new FormData();
                    formData.append('Name', values.name);
                    formData.append('Image', values.file);
                    const resp = await brandService.updateBrand(token, selectedBrand.id, formData);
                    if (resp.isSuccess) {
                        setIsLoading(false);
                        toast.success('Cập nhật dữ liệu thành công');
                        fetchData();
                    }
                } catch (error) {
                    setIsLoading(false);
                    resetForm();
                    if (error.response && error.response.data && error.response.data.messages) {
                        const errorMessages = error.response.data.messages;
                        toast.error(errorMessages.join(', '));
                    } else {
                        toast.error('Có lỗi xảy ra.');
                    }
                }
            }
        } else {
            try {
                closeDialog();
                const formData = new FormData();
                formData.append('Name', values.name);
                formData.append('Image', values.file);

                const resp = await brandService.addNewBrand(token, formData);
                setIsLoading(false);

                if (resp.isSuccess) {
                    toast.success('Thêm dữ liệu thành công');
                    fetchData();
                }
            } catch (error) {
                setIsLoading(false);
                if (error.response && error.response.data && error.response.data.message) {
                    toast.error(error.response.data.message);
                } else {
                    toast.error('An unexpected error occurred.');
                }
            }
        }
    };

    const formik = useFormik({
        initialValues: {
            name: '',
            file: '',
        },
        validationSchema: isUpdateMode
            ? Yup.object({
                  name: Yup.string().required('Vui lòng nhập thông tin!'),
              })
            : Yup.object({
                  name: Yup.string().required('Vui lòng nhập thông tin!'),
                  file: Yup.mixed()
                      .required('Vui lòng tải lên hình ảnh!')
                      .test(
                          'fileFormat',
                          'Định dạng tệp không hợp lệ. Chỉ chấp nhận các định dạng như jpg, jpeg, png, gif.',
                          (value) => {
                              if (!value) return true;
                              const acceptedFormats = ['image/jpg', 'image/jpeg', 'image/png', 'image/gif'];
                              return acceptedFormats.includes(value.type);
                          },
                      ),
              }),
        onSubmit: handleSubmit,
    });
    const columns = [
        {
            name: 'ID',
            selector: (row) => row.id,
            sortable: true,
        },
        {
            name: 'Hình ảnh',
            selector: (row) => <img src={row.image} className="w-14 h-14 my-2"></img>,
            sortable: false,
        },
        {
            name: 'Tên thương hiệu',
            selector: (row) => <div className="text-sm">{row.name}</div>,
            sortable: false,
        },

        {
            name: 'Actions',
            cell: (row) => (
                <>
                    <button
                        className="btn btn-outline btn-success m-2"
                        onClick={() => {
                            handleUpdate(row.id);
                        }}
                    >
                        <FolderEdit />
                    </button>
                    <button
                        className="btn btn-outline btn-error mx-2"
                        onClick={() => {
                            handleDeleteModal(row.id);
                        }}
                    >
                        <Trash />
                    </button>
                </>
            ),
        },
    ];
    return (
        <div className="m-10">
            {isLoading ? (
                <Loading></Loading>
            ) : (
                <>
                    <dialog id="dialog_confirm" className="modal">
                        <div className="modal-box max-w-lg">
                            <h3 className="font-bold text-2xl text-center">XÓA THƯƠNG HIỆU</h3>
                            <form className="my-2" onSubmit={handleDelete}>
                                <div
                                    onClick={() => document.getElementById('dialog_confirm').close()}
                                    className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2"
                                >
                                    X
                                </div>
                                <div className="text-center space-x-10">
                                    <p className="my-10">Bạn chắc chắn xóa?</p>
                                    <div className="flex items-center mt-3 text-center justify-center">
                                        <button className="btn btn-primary text-white">Xác nhận</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </dialog>
                    <dialog id="dialog" className="modal">
                        <div className="modal-box max-w-2xl">
                            <h3 className="font-bold text-2xl text-center">THƯƠNG HIỆU</h3>
                            <form className="my-2" onSubmit={formik.handleSubmit}>
                                <div
                                    onClick={closeDialog}
                                    className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2"
                                >
                                    X
                                </div>
                                <div className="text-center space-x-10">
                                    <div>
                                        <FormInput
                                            type="text"
                                            label="Tên Thương hiệu"
                                            name="name"
                                            value={formik.values.name}
                                            placeholder="Nhập tên thương hiệu..."
                                            onchange={formik.handleChange}
                                        />
                                        {formik.errors.name && (
                                            <span className="text-error text-sm p-1 ">{formik.errors.name}</span>
                                        )}
                                        <FormInput
                                            type="file"
                                            label="Hình ảnh"
                                            name="file"
                                            onchange={(e) => formik.setFieldValue('file', e.target.files[0])}
                                        />
                                        {formik.errors.file && (
                                            <p className="text-error text-sm p-1">{formik.errors.file}</p>
                                        )}
                                        {formik.values.file && <PreviewImage file={formik.values.file} />}
                                        <div className="flex items-center mt-3 text-center justify-center">
                                            <SubmitButton text={isUpdateMode ? 'Cập nhật' : 'Thêm'} color="primary" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </dialog>
                    <div className="container">
                        <DataTable
                            title="QUẢN LÝ THƯƠNG HIỆU"
                            fixedHeader
                            fixedHeaderScrollHeight="550px"
                            direction="auto"
                            responsive
                            pagination
                            columns={columns}
                            data={filteredItems}
                            striped
                            subHeader
                            paginationResetDefaultPage={resetPaginationToggle}
                            subHeaderComponent={subHeaderComponentMemo}
                            persistTableHead
                            progressPending={pending}
                            progressComponent={<TableLoader />}
                            customStyles={{
                                table: {
                                    fontSize: '30px',
                                },
                            }}
                        />
                    </div>
                </>
            )}
        </div>
    );
};

export default BrandManagement;
