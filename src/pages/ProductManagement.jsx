import React, { useEffect, useState, useMemo } from 'react';
import DataTable from 'react-data-table-component';
import { FormInput, Loading, SubmitButton, TableLoader } from '../components';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import { toast } from 'react-toastify';
import { useSelector, useDispatch } from 'react-redux';
import { FolderEdit, Trash } from 'lucide-react';
import { formatPrice } from '../utils/helpers';
import productService from '../services/productService';
import categoryService from '../services/categoryService';
import brandService from '../services/brandService';

const ProductManagement = () => {
    const navigate = useNavigate();
    const [data, setData] = useState([]);
    const [isUpdateMode, setIsUpdateMode] = useState(false);
    const [categoyList, setCategoryList] = useState([]);
    const [brandList, setBrandList] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [pending, setPending] = useState(true);
    const [selectedProduct, setSelectedProduct] = useState(null);
    const [selectedProductId, setSelectedProductId] = useState(null);
    const [productAttributesList, setProductAttributesList] = useState([]);
    const [saveSuccess, setSaveSuccess] = useState(false);
    const [selectedAttributes, setSelectedAttributes] = useState({});
    const [productAttributes, setProductAttributes] = useState([]);
    const [selectedCategoryId, setSelectedCategoryId] = useState('');
    const token = useSelector((state) => state.auth.loginAdmin?.token);

    useEffect(() => {
        if (!token) {
            navigate('/auth/admin/login');
        } else {
            fetchData();
        }
    }, []);

    const fetchData = async () => {
        setPending(true);
        try {
            const respProducts = await productService.getAllProducts(token);
            const respCategories = await categoryService.getAllCategories(token);
            const respBrands = await brandService.getAllBrands(token);
            setData(respProducts.data);
            setCategoryList(respCategories.data);
            setBrandList(respBrands.data);
            setPending(false);
        } catch (error) {
            console.log(error);
        }
    };

    //Search
    const [filterText, setFilterText] = useState('');
    const [resetPaginationToggle, setResetPaginationToggle] = useState(false);
    const filteredItems = data.filter(
        (item) => item.name && item.name.toLowerCase().includes(filterText.toLowerCase()),
    );
    const subHeaderComponentMemo = useMemo(() => {
        return (
            <div className="grid grid-cols-2 my-2">
                <div className="relative">
                    <label className="input-group w-full">
                        <input
                            value={filterText}
                            onChange={(e) => setFilterText(e.target.value)}
                            placeholder="Nhập từ khóa tìm kiếm..."
                            className="input input-bordered max-w-xs"
                        />
                    </label>
                </div>
                <div className="flex items-center mx-5">
                    <button
                        className="btn bg-primary btn-ghost text-white"
                        onClick={() => {
                            setIsUpdateMode(false);
                            document.getElementById('dialog').showModal();
                        }}
                    >
                        + Thêm sản phẩm
                    </button>
                </div>
            </div>
        );
    }, [filterText, resetPaginationToggle]);

    const closeDialog = () => {
        document.getElementById('dialog').close();
        resetForm();
    };

    const resetForm = () => {
        setSaveSuccess(false);
        setSelectedCategoryId(null);
        setSelectedAttributes({});
        setProductAttributesList([]);
        setProductAttributes([]);
        setSelectedProductId(null);
        setSelectedProduct([]);
        formik.resetForm();
    };
    const handleDeleteModal = (id) => {
        document.getElementById('dialog_confirm').showModal();
        setSelectedProductId(id);
    };
    const handleDelete = async () => {
        setIsLoading(true);
        try {
            const resp = await productService.deleteProduct(token, selectedProductId);
            if (resp.isSuccess) {
                setIsLoading(false);
                toast.success('Xóa dữ liệu thành công!');
                fetchData();
            }
        } catch (error) {
            setIsLoading(false);
            if (error.response && error.response.data && error.response.data.message) {
                toast.error(error.response.data.message);
            } else {
                toast.error('An unexpected error occurred.');
            }
        }
    };
    const handleUpdate = (id) => {
        setIsUpdateMode(true);
        getProduct(id);
        document.getElementById('dialog').showModal();
    };

    const getProduct = async (id) => {
        const resp = await productService.getProductById(id);
        setSelectedProduct(resp.data);
    };

    const handleCategoryChange = async (categoryId) => {
        setSelectedCategoryId(categoryId);
        const resp = await productService.getProductAttributeByCategory(token, categoryId);
        setProductAttributes(resp.data);
    };

    const handleAttributeChange = (attributeId, isChecked) => {
        setSelectedAttributes((prevAttributes) => {
            if (isChecked) {
                return { ...prevAttributes, [attributeId]: '' };
            } else {
                const { [attributeId]: removedAttribute, ...rest } = prevAttributes;
                return rest;
            }
        });
    };

    const handleSubmit = async (values) => {
        setIsLoading(true);
        if (isUpdateMode) {
            if (selectedProduct) {
                try {
                    closeDialog();
                    const info = {
                        id: selectedProduct?.id,
                        color: values.color !== '' ? values.color : selectedProduct?.color,
                        description: values.description !== '' ? values.description : selectedProduct?.description,
                        guarantee: values.guarantee !== '' ? values.guarantee : selectedProduct?.guarantee,
                        name: values.name !== '' ? values.name : selectedProduct?.name,
                        onSale: values.onSale !== '' ? values.onSale : selectedProduct?.onSale,
                        status: values.status !== '' ? values.status : selectedProduct?.status,
                        price: values.price !== '' ? values.price : selectedProduct?.price,
                        quantity:
                            values.quantity !== '' && quantity !== '' ? values.quantity : selectedProduct?.quantity,
                        salePrice:
                            values.salePrice !== '' && salePrice !== '' ? values.salePrice : selectedProduct?.salePrice,
                        brandId: values.brandId !== '' && brandId !== '' ? values.brandId : selectedProduct?.brandId,
                        categoryId:
                            values.categoryId !== '' && categoryId !== ''
                                ? values.categoryId
                                : selectedProduct?.categoryId,
                    };

                    var formData = new FormData();
                    formData.append('info', JSON.stringify(info));
                    for (let i = 0; values.images && i < values.images.length; i++) {
                        formData.append('images', values.images[i]);
                    }
                    formData.append('productSpecsString', JSON.stringify(productAttributesList));

                    const resp = await productService.updateProduct(token, formData);

                    if (resp.isSuccess) {
                        setIsLoading(false);
                        toast.success('Cập nhật dữ liệu thành công!');
                        fetchData();
                    } else {
                        toast.error(resp.message);
                    }
                } catch (error) {
                    setIsLoading(false);
                    if (error.response && error.response.data && error.response.data.messages) {
                        const errorMessages = error.response.data.messages;
                        toast.error(errorMessages.join(', '));
                    } else {
                        toast.error('Có lỗi xảy ra.');
                    }
                }
            }
        } else {
            try {
                closeDialog();
                const info = {
                    color: values.color,
                    description: values.description,
                    guarantee: values.guarantee,
                    name: values.name,
                    onSale: values.onSale,
                    status: values.status,
                    price: values.price,
                    quantity: values.quantity,
                    salePrice: values.salePrice,
                    brandId: values.brandId,
                    categoryId: values.categoryId,
                };

                var formData = new FormData();

                formData.append('info', JSON.stringify(info));
                for (let i = 0; i < values.images.length; i++) {
                    formData.append('images', values.images[i]);
                }
                formData.append('productSpecsString', JSON.stringify(productAttributesList));
                const resp = await productService.addNewProduct(token, formData);

                if (resp.isSuccess) {
                    setIsLoading(false);
                    toast.success('Dữ liệu đã được thêm!');
                    fetchData();
                } else {
                    toast.error(resp.message);
                }
            } catch (error) {
                setIsLoading(false);
                if (error.response && error.response.data && error.response.data.messages) {
                    const errorMessages = error.response.data.messages;
                    toast.error(errorMessages.join(', '));
                } else {
                    toast.error('Có lỗi xảy ra.');
                }
            }
        }
    };
    const columns = [
        {
            name: 'ID',
            selector: (row) => row.id,
            sortable: true,
            width: '300px',
        },
        {
            name: 'Hình ảnh',
            selector: (row) => <img src={row.images[0]} className="w-20 h-20 my-2"></img>,
            sortable: false,
            width: '100px',
        },
        {
            name: 'Tên',
            selector: (row) => <div className="text-sm">{row.name}</div>,
            sortable: false,
            width: '400px',
        },
        {
            name: 'Bảo hành',
            selector: (row) => <div className="text-sm">{row.guarantee}</div>,
            sortable: false,
            width: '100px',
        },
        {
            name: 'Giá',
            selector: (row) => <div className="text-sm">{formatPrice(row.price)}</div>,
            sortable: false,
            width: '140px',
        },
        {
            name: 'Số lượng',
            selector: (row) => <div className="text-sm">{row.quantity}</div>,
            sortable: false,
            width: '100px',
        },
        {
            name: 'Đã bán',
            selector: (row) => <div className="text-sm">{row.sold}</div>,
            sortable: false,
            width: '100px',
        },
        {
            name: 'Actions',
            cell: (row) => (
                <>
                    <button
                        className="btn btn-outline btn-success m-2"
                        onClick={() => {
                            handleUpdate(row.id);
                        }}
                    >
                        <FolderEdit />
                    </button>
                    <button
                        className="btn btn-outline btn-error mx-2"
                        onClick={() => {
                            handleDeleteModal(row.id);
                        }}
                    >
                        <Trash />
                    </button>
                </>
            ),
            width: '100px',
        },
    ];

    const formik = useFormik({
        initialValues: {
            name: '',
            color: '',
            description: '',
            guarantee: '',
            onSale: false,
            price: 0,
            quantity: 0,
            salePrice: 0,
            status: '',
            brandId: '',
            categoryId: '',
            images: [],
            productSpecsString: '',
        },
        validationSchema: isUpdateMode
            ? Yup.object({
                  quantity: Yup.number().required('Vui lòng nhập thông tin!').min(0, 'Số lượng không được âm'),
                  salePrice: Yup.number().required('Vui lòng nhập thông tin!').min(0, 'Giá bán không được âm'),
                  price: Yup.number().required('Vui lòng nhập thông tin!').min(0, 'Giá không được âm'),
              })
            : Yup.object({
                  name: Yup.string().required('Vui lòng nhập thông tin!'),
                  color: Yup.string().required('Vui lòng nhập thông tin!'),
                  guarantee: Yup.string().required('Vui lòng nhập thông tin!'),
                  description: Yup.string().required('Vui lòng nhập thông tin!'),
                  onSale: Yup.boolean().required('Vui lòng nhập thông tin!'),
                  quantity: Yup.number().required('Vui lòng nhập thông tin!').min(0, 'Số lượng không được âm'),
                  salePrice: Yup.number().required('Vui lòng nhập thông tin!').min(0, 'Giá bán không được âm'),
                  price: Yup.number().required('Vui lòng nhập thông tin!').min(0, 'Giá không được âm'),
                  brandId: Yup.string().required('Vui lòng nhập thông tin!'),
                  categoryId: Yup.string().required('Vui lòng nhập thông tin!'),
                  images: Yup.mixed().required('Vui lòng tải lên hình ảnh!'),
              }),
        onSubmit: handleSubmit,
    });

    const ExpandedComponent = ({ data }) => {
        return (
            <div className="mx-20 grid grid-cols-2 gap-1">
                <div className="mb-2">
                    <strong>ID:</strong> {data.id}
                </div>
                <div className="mb-2">
                    <strong>Hình ảnh:</strong>
                    <div className="flex">
                        {data.images.map((image, index) => (
                            <img key={index} src={image} className="w-20 h-20" alt={`Product ${index + 1}`} />
                        ))}
                    </div>
                </div>
                <div className="mb-2">
                    <strong>Tên:</strong> <div className="text-sm">{data.name}</div>
                </div>
                <div className="mb-2">
                    <strong>Mô tả:</strong> <div className="text-sm">{data.description}</div>
                </div>
                <div>
                    <strong>Thuộc tính:</strong>
                    {data.productSpecs.map((spec) => (
                        <div key={spec.id} className="my-1 text-sm">
                            {JSON.parse(spec.value).map((item, index) => (
                                <div key={index}>{item}</div>
                            ))}
                        </div>
                    ))}
                </div>
            </div>
        );
    };
    return (
        <div className="m-10">
            {isLoading ? (
                <Loading />
            ) : (
                <>
                    <dialog id="dialog_confirm" className="modal">
                        <div className="modal-box max-w-lg">
                            <h3 className="font-bold text-2xl text-center">XÓA THƯƠNG HIỆU</h3>
                            <form className="my-2" onSubmit={handleDelete}>
                                <div
                                    onClick={() => document.getElementById('dialog_confirm').close()}
                                    className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2"
                                >
                                    X
                                </div>
                                <div className="text-center space-x-10">
                                    <p className="my-10">Bạn chắc chắn xóa?</p>
                                    <div className="flex items-center mt-3 text-center justify-center">
                                        <button className="btn btn-primary text-white">Xác nhận</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </dialog>
                    <dialog id="dialog" className="modal">
                        <div className="modal-box max-w-6xl">
                            <h3 className="font-bold text-2xl text-center">SẢN SẢN PHẨM MỚI</h3>
                            <form className="my-2" onSubmit={formik.handleSubmit}>
                                <div className="my-2 grid grid-cols-3 space-x-5">
                                    <div
                                        onClick={closeDialog}
                                        className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2"
                                    >
                                        X
                                    </div>
                                    <div className="space-x-10">
                                        <div>
                                            <FormInput
                                                type="text"
                                                label="Tên Sản phẩm"
                                                name="name"
                                                value={formik.values.name}
                                                placeholder="Nhập tên danh mục..."
                                                onchange={formik.handleChange}
                                            />
                                            {formik.errors.name && (
                                                <span className="text-error text-sm p-1 ">{formik.errors.name}</span>
                                            )}
                                            <FormInput
                                                type="text"
                                                label="Mô tả"
                                                name="description"
                                                value={formik.values.description}
                                                placeholder="Nhập mô tả..."
                                                onchange={formik.handleChange}
                                            />
                                            {formik.errors.description && (
                                                <span className="text-error text-sm p-1 ">
                                                    {formik.errors.description}
                                                </span>
                                            )}
                                            <FormInput
                                                type="text"
                                                label="Màu sắc"
                                                name="color"
                                                value={formik.values.color}
                                                placeholder="Nhập màu sắc..."
                                                onchange={formik.handleChange}
                                            />
                                            {formik.errors.color && (
                                                <span className="text-error text-sm p-1 ">{formik.errors.color}</span>
                                            )}
                                            <FormInput
                                                type="text"
                                                label="Bảo hành"
                                                name="guarantee"
                                                value={formik.values.guarantee}
                                                placeholder="Nhập thời gian..."
                                                onchange={formik.handleChange}
                                            />
                                            {formik.errors.guarantee && (
                                                <span className="text-error text-sm p-1 ">
                                                    {formik.errors.guarantee}
                                                </span>
                                            )}
                                            <FormInput
                                                type="file"
                                                label="Hình ảnh sản phẩm"
                                                name="images"
                                                multiple="true"
                                                onchange={(e) => formik.setFieldValue('images', e.target.files)}
                                            />
                                            {formik.errors.images && (
                                                <p className="text-error text-sm p-1">{formik.errors.images}</p>
                                            )}
                                            <div className="flex items-center mt-3 text-center justify-center">
                                                <SubmitButton
                                                    text={isUpdateMode ? 'Cập nhật' : 'Thêm'}
                                                    color="primary"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <FormInput
                                            type="text"
                                            label="Giá sản phẩm"
                                            name="price"
                                            value={formik.values.price}
                                            placeholder="Nhập giá..."
                                            onchange={formik.handleChange}
                                        />
                                        {formik.errors.price && (
                                            <span className="text-error text-sm p-1 ">{formik.errors.price}</span>
                                        )}
                                        <FormInput
                                            type="text"
                                            label="Giá giảm"
                                            name="salePrice"
                                            value={formik.values.salePrice}
                                            placeholder="Nhập giá..."
                                            onchange={formik.handleChange}
                                        />
                                        {formik.errors.salePrice && (
                                            <span className="text-error text-sm p-1 ">{formik.errors.salePrice}</span>
                                        )}
                                        <div className="flex justify-around">
                                            <FormInput
                                                type="checkbox"
                                                label="Giảm giá"
                                                name="onSale"
                                                checked={formik.values.onSale}
                                                onchange={formik.handleChange}
                                            />
                                            {formik.errors.onSale && (
                                                <span className="text-error text-sm p-1 ">{formik.errors.onSale}</span>
                                            )}
                                            <FormInput
                                                type="checkbox"
                                                label="Trạng thái"
                                                name="status"
                                                checked={formik.values.status}
                                                onchange={formik.handleChange}
                                            />
                                            {formik.errors.status && (
                                                <span className="text-error text-sm p-1 ">{formik.errors.status}</span>
                                            )}
                                        </div>
                                        <FormInput
                                            type="text"
                                            label="Số lượng"
                                            name="quantity"
                                            value={formik.values.quantity}
                                            placeholder="Nhập giá..."
                                            onchange={formik.handleChange}
                                        />
                                        {formik.errors.quantity && (
                                            <span className="text-error text-sm p-1 ">{formik.errors.quantity}</span>
                                        )}
                                        <div className="my-4">
                                            <label className="block text-sm">Chọn Danh mục</label>
                                            <select
                                                name="categoryId"
                                                value={formik.values.categoryId}
                                                onChange={(e) => {
                                                    formik.handleChange(e);
                                                    handleCategoryChange(e.target.value);
                                                }}
                                                className="select select-bordered mt-1 p-2 rounded-md w-full"
                                            >
                                                <option value="">-- Chọn danh mục --</option>
                                                {categoyList.map((category) => (
                                                    <option key={category.id} value={category.id}>
                                                        {category.name}
                                                    </option>
                                                ))}
                                            </select>
                                            {formik.errors.categoryId && (
                                                <span className="text-error text-sm p-1 ">
                                                    {formik.errors.categoryId}
                                                </span>
                                            )}
                                        </div>
                                        <div className="my-4">
                                            <label className="block text-sm">Chọn Thương hiệu</label>
                                            <select
                                                name="brandId"
                                                value={formik.values.brandId}
                                                onChange={formik.handleChange}
                                                className="select select-bordered mt-1 p-2 rounded-md w-full"
                                            >
                                                <option value="">-- Chọn thương hiệu --</option>
                                                {brandList.map((brand) => (
                                                    <option key={brand.id} value={brand.id}>
                                                        {brand.name}
                                                    </option>
                                                ))}
                                            </select>
                                        </div>
                                        {formik.errors.brandId && (
                                            <span className="text-error text-sm p-1 ">{formik.errors.brandId}</span>
                                        )}
                                    </div>

                                    <div>
                                        <p>Thuộc tính sản phẩm</p>
                                        <div className="my-4">
                                            {productAttributesList &&
                                                productAttributes.map((attribute) => (
                                                    <div key={attribute.id} className="">
                                                        <input
                                                            type="checkbox"
                                                            id={`attribute-${attribute.id}`}
                                                            onChange={(e) =>
                                                                handleAttributeChange(attribute.id, e.target.checked)
                                                            }
                                                            checked={selectedAttributes[attribute.id] !== undefined}
                                                            className="mr-2"
                                                        />
                                                        <label htmlFor={`attribute-${attribute.id}`}>
                                                            {attribute.name}
                                                        </label>
                                                        {selectedAttributes[attribute.id] !== undefined && (
                                                            <FormInput
                                                                type="text"
                                                                placeholder={`Nhập thông số của ${attribute.name}`}
                                                                value={selectedAttributes[attribute.id]}
                                                                onchange={(e) =>
                                                                    setSelectedAttributes((prevAttributes) => ({
                                                                        ...prevAttributes,
                                                                        [attribute.id]: e.target.value,
                                                                    }))
                                                                }
                                                            />
                                                        )}
                                                    </div>
                                                ))}
                                            <div className="">
                                                <button
                                                    type="button"
                                                    onClick={() => {
                                                        const formattedAttributes = Object.entries(
                                                            selectedAttributes,
                                                        ).map(([productAttributeId, value]) => ({
                                                            productAttributeId,
                                                            value: value
                                                                ? JSON.stringify(
                                                                      value.split(',').map((item) => item.trim()),
                                                                  )
                                                                : '[]',
                                                        }));
                                                        setProductAttributesList(formattedAttributes);
                                                        setSaveSuccess(true);
                                                    }}
                                                    className="btn bg-primary text-white"
                                                >
                                                    Lưu
                                                </button>
                                                {saveSuccess && <p className="text-success">Đã lưu thành công!</p>}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </dialog>
                    <div className="container">
                        <DataTable
                            title="QUẢN LÝ SẢN PHẨM"
                            fixedHeader
                            fixedHeaderScrollHeight="550px"
                            direction="auto"
                            responsive
                            pagination
                            columns={columns}
                            data={filteredItems}
                            striped
                            subHeader
                            paginationResetDefaultPage={resetPaginationToggle}
                            subHeaderComponent={subHeaderComponentMemo}
                            persistTableHead
                            expandableRows
                            expandableRowsComponent={ExpandedComponent}
                            progressPending={pending}
                            progressComponent={<TableLoader />}
                            customStyles={{
                                table: {
                                    fontSize: '30px',
                                },
                            }}
                        />
                    </div>
                </>
            )}
        </div>
    );
};

export default ProductManagement;
