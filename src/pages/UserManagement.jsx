import React, { useEffect, useState, useMemo } from 'react';
import DataTable from 'react-data-table-component';
import { TableLoader, Loading, SubmitButton } from '../components';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useSelector } from 'react-redux';
import { FolderEdit } from 'lucide-react';
import userService from '../services/userService';
import { FaCheckCircle, FaBan } from 'react-icons/fa';

const UserManagement = () => {
    const navigate = useNavigate();
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [pending, setPending] = useState(true);
    const [selectedUserId, setSelectedUserId] = useState(null);
    const [newStatus, setNewStatus] = useState(null);
    const token = useSelector((state) => state.auth.loginAdmin?.token);

    useEffect(() => {
        if (!token) {
            navigate('/auth/admin/login');
        } else {
            fetchData();
        }
    }, []);

    const fetchData = async () => {
        setPending(true);
        try {
            const resp = await userService.getAllUsers(token);
            setData(resp.data);
            setPending(false);
        } catch (error) {
            console.log(error);
        }
    };

    //Search
    const [filterText, setFilterText] = useState('');
    const [resetPaginationToggle, setResetPaginationToggle] = useState(false);
    const filteredItems = data.filter(
        (item) => item.fullName && item.fullName.toLowerCase().includes(filterText.toLowerCase()),
    );
    const subHeaderComponentMemo = useMemo(() => {
        return (
            <div className="my-2">
                <div className="relative">
                    <label className="input-group w-full">
                        <input
                            value={filterText}
                            onChange={(e) => setFilterText(e.target.value)}
                            placeholder="Nhập tên"
                            className="input input-bordered max-w-xs"
                        />
                    </label>
                </div>
            </div>
        );
    }, [filterText, resetPaginationToggle]);

    const closeDialog = () => {
        document.getElementById('dialog').close();
    };

    const handleUpdate = async (id, currentStatus) => {
        document.getElementById('dialog').showModal();
        setSelectedUserId(id);
        setNewStatus(!currentStatus);
    };

    const confirmStatusUpdate = async () => {
        setIsLoading(true);
        try {
            closeDialog();
            const resp = await userService.updateUser(token, selectedUserId, newStatus);
            if (resp.isSuccess) {
                setIsLoading(false);
                toast.success('Thay đổi thành công!');
                fetchData();
            }
        } catch (error) {
            setIsLoading(false);
            if (error.response && error.response.data && error.response.data.messages) {
                const errorMessages = error.response.data.messages;
                toast.error(errorMessages.join(', '));
            } else {
                toast.error('Có lỗi xảy ra.');
            }
        }
    };

    const columns = [
        {
            name: 'ID',
            selector: (row) => row.id,
            sortable: true,
        },
        {
            name: 'Email',
            selector: (row) => <div>{row.email}</div>,
            sortable: false,
        },
        {
            name: 'Tên',
            selector: (row) => <div className="text-sm">{row.fullName}</div>,
            sortable: false,
        },
        {
            name: 'Vai trò',
            selector: (row) => <div className="text-sm">{row.role}</div>,
            sortable: false,
            width: '120px',
        },
        {
            name: 'Trạng thái',
            selector: (row) => (
                <div className="">
                    {row.status ? (
                        <FaCheckCircle className="text-success w-6 h-6" />
                    ) : (
                        <FaBan className="text-error w-6 h-6" />
                    )}
                </div>
            ),
            sortable: false,
        },

        {
            name: 'Actions',
            cell: (row) => (
                <>
                    <button
                        className="btn btn-outline btn-success m-2"
                        onClick={() => handleUpdate(row.id, row.status)}
                    >
                        <FolderEdit />
                    </button>
                </>
            ),
        },
    ];
    return (
        <div className="m-10">
            {isLoading ? (
                <Loading />
            ) : (
                <>
                    <dialog id="dialog" className="modal">
                        <div className="modal-box max-w-2xl">
                            <h3 className="font-bold text-2xl text-center">CẬP NHẬT TRẠNG THÁI NGƯỜI DÙNG</h3>
                            <form className="my-2" onSubmit={confirmStatusUpdate}>
                                <div
                                    onClick={closeDialog}
                                    className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2"
                                >
                                    X
                                </div>
                                <div className="text-center space-x-10">
                                    <p className="my-10">Bạn muốn cập nhật lại trạng thái người dùng?</p>
                                    <div className="flex items-center mt-3 text-center justify-center">
                                        <SubmitButton text="Cập nhật" color="primary" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </dialog>

                    <div className="container">
                        <DataTable
                            title="QUẢN LÝ NGƯỜI DÙNG"
                            fixedHeader
                            fixedHeaderScrollHeight="550px"
                            direction="auto"
                            responsive
                            pagination
                            columns={columns}
                            data={filteredItems}
                            striped
                            subHeader
                            paginationResetDefaultPage={resetPaginationToggle}
                            subHeaderComponent={subHeaderComponentMemo}
                            progressPending={pending}
                            progressComponent={<TableLoader />}
                            persistTableHead
                            customStyles={{
                                table: {
                                    fontSize: '30px',
                                },
                            }}
                        />
                    </div>
                </>
            )}
        </div>
    );
};

export default UserManagement;
