import axiosClient from '../utils/axiosClient';

const categoryService = {
    addNewCategory(accessToken, formData) {
        const url = '/admin/category';
        return axiosClient.post(url, formData, {
            headers: {
                accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${accessToken}`,
            },
        });
    },
    getAllCategories(accessToken) {
        const url = '/admin/category';
        return axiosClient.get(url, {
            headers: {
                accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${accessToken}`,
            },
        });
    },
    getCategoryById(id) {
        const url = `/category/${id}`;
        return axiosClient.get(url);
    },
    updateCategory(accessToken, id, formData) {
        const url = `/admin/category/${id}`;
        return axiosClient.patch(url, formData, {
            headers: {
                accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${accessToken}`,
            },
        });
    },
    deleteCategory(accessToken, id) {
        const url = `/admin/category/${id}`;
        return axiosClient.delete(url, {
            headers: {
                accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${accessToken}`,
            },
        });
    },
};
export default categoryService;
