import axiosClient from '../utils/axiosClient';

const authServices = {
    login(email, password) {
        const url = '/auth/login';
        return axiosClient.post(url, { email, password });
    },
};

export default authServices;
