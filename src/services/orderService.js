import axiosClient from '../utils/axiosClient';

const orderService = {
    getAllOrders(accessToken) {
        const url = '/admin/order';
        return axiosClient.get(url, {
            headers: {
                accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${accessToken}`,
            },
        });
    },

    updateOrderStatus(accessToken, id, status) {
        const url = `/admin/order/${id}`;
        return axiosClient.patch(
            url,
            { status },
            {
                headers: {
                    accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${accessToken}`,
                },
            },
        );
    },
    // updatePaid(accessToken, id) {
    //     const url = `/order/paid/${id}`;
    //     return axiosClient.patch(url, {
    //         headers: {
    //             accept: 'application/json',
    //             'Content-Type': 'application/json',
    //             Authorization: `Bearer ${accessToken}`,
    //         },
    //     });
    // },
    updatePaid(id) {
        const url = `/order/paid/${id}`;
        return axiosClient.patch(url);
    },
};
export default orderService;
