import axiosClient from '../utils/axiosClient';

const productService = {
    addNewProduct(accessToken, formData) {
        const url = '/admin/product';
        return axiosClient.post(url, formData, {
            headers: {
                accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${accessToken}`,
            },
        });
    },
    getAllProducts(accessToken) {
        const url = '/admin/product';
        return axiosClient.get(url, {
            headers: {
                accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${accessToken}`,
            },
        });
    },
    getProductById(id) {
        const url = `/product/${id}`;
        return axiosClient.get(url);
    },
    getProductAttributeByCategory(accessToken, id) {
        const url = `/admin/product-attribute/by-category?categoryId=${id}`;
        return axiosClient.get(url, {
            headers: {
                accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${accessToken}`,
            },
        });
    },
    updateProduct(accessToken, formData) {
        const url = '/admin/product';
        return axiosClient.patch(url, formData, {
            headers: {
                accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${accessToken}`,
            },
        });
    },
    deleteProduct(accessToken, id) {
        const url = `/admin/product/${id}`;
        return axiosClient.delete(url, {
            headers: {
                accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${accessToken}`,
            },
        });
    },
};
export default productService;
