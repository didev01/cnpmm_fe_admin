import axiosClient from '../utils/axiosClient';

const brandService = {
    addNewBrand(accessToken, formData) {
        const url = '/admin/brand';
        return axiosClient.post(url, formData, {
            headers: {
                accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${accessToken}`,
            },
        });
    },
    getAllBrands(accessToken) {
        const url = '/admin/brand';
        return axiosClient.get(url, {
            headers: {
                accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${accessToken}`,
            },
        });
    },
    getBrandById(id) {
        const url = `/brand/${id}`;
        return axiosClient.get(url);
    },
    updateBrand(accessToken, id, formData) {
        const url = `/admin/brand/${id}`;
        return axiosClient.patch(url, formData, {
            headers: {
                accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${accessToken}`,
            },
        });
    },
    deleteBrand(accessToken, id) {
        const url = `/admin/brand/${id}`;
        return axiosClient.delete(url, {
            headers: {
                accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${accessToken}`,
            },
        });
    },
};
export default brandService;
