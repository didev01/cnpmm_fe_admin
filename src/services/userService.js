import axiosClient from '../utils/axiosClient';

const userService = {
    getAllUsers(accessToken) {
        const url = '/admin/user';
        return axiosClient.get(url, {
            headers: {
                accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${accessToken}`,
            },
        });
    },
    updateUser(accessToken, id, status) {
        const url = `/admin/user/${id}`;
        return axiosClient.patch(
            url,
            { status },
            {
                headers: {
                    accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${accessToken}`,
                },
            },
        );
    },
};

export default userService;
